#!/bin/sh
set -e

a=/usr/local/etc/php/php.ini

echo "Generating config php.ini started"

echo "engine =on
short_open_tag =${CFG_SHORT_OPEN_TAG}
asp_tags =${CFG_ASP_TAGS}
precision =${CFG_PRECISION}
output_buffering =${CFG_OUTPUT_BUFFERING}
zlib.output_compression =${CFG_ZLIB_OUTPUT_COMPRESSION}
implicit_flush =${CFG_IMPLICIT_FLUSH}
unserialize_callback_func =${CFG_UNSERIALIZE_CALLBACK_FUNC}
serialize_precision =${CFG_SERIALIZE_PRECISION}
disable_functions =${CFG_DISABLE_FUNCTIONS}
disable_classes =${CFG_DISABLE_CLASSES}
zend.enable_gc =${CFG_ZEND_ENABLE_GC}
expose_php =${CFG_EXPOSE_PHP}
max_execution_time =${CFG_MAX_EXECUTION_TIME}
max_input_time =${CFG_MAX_INPUT_TIME}
memory_limit =${CFG_MEMORY_LIMIT}
error_reporting =${CFG_ERROR_REPORTING}
display_errors =${CFG_DISPLAY_ERRORS}
display_startup_errors =${CFG_DISPLAY_STARTUP_ERRORS}
log_errors =${CFG_LOG_ERRORS}
log_errors_max_len =${CFG_LOG_ERRORS_MAX_LEN}
ignore_repeated_errors =${CFG_IGNORE_REPEATED_ERRORS}
ignore_repeated_source =${CFG_IGNORE_REPEATED_SOURCE}
report_memleaks =${CFG_REPORT_MEMLEAKS}
track_errors =${CFG_TRACK_ERRORS}
html_errors =${CFG_HTML_ERRORS}
variables_order =${CFG_VARIABLES_ORDER}
request_order =${CFG_REQUEST_ORDER}
register_argc_argv =${CFG_REGISTER_ARGC_ARGV}
auto_globals_jit =${CFG_AUTO_GLOBALS_JIT}
post_max_size =${CFG_POST_MAX_SIZE}
auto_prepend_file =${CFG_AUTO_PREPEND_FILE}
auto_append_file =${CFG_AUTO_APPEND_FILE}
default_mimetype =${CFG_DEFAULT_MIMETYPE}
default_charset =${CFG_DEFAULT_CHARSET}
doc_root =${CFG_DOC_ROOT}
user_dir =${CFG_USER_DIR}
enable_dl =${CFG_ENABLE_DL}
file_uploads =${CFG_FILE_UPLOADS}
upload_max_filesize =${CFG_UPLOAD_MAX_FILESIZE}
max_file_uploads =${CFG_MAX_FILE_UPLOADS}
allow_url_fopen =${CFG_ALLOW_URL_FOPEN}
allow_url_include =${CFG_ALLOW_URL_INCLUDE}
default_socket_timeout =${CFG_DEFAULT_SOCKET_TIMEOUT}\n" > $a


/usr/sbin/apache2 -DFOREGROUND

exec "$@"