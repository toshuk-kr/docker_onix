-- Create Table
DROP TABLE IF EXISTS `USERS`;
CREATE TABLE `USERS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `AGE` int(11) DEFAULT NULL,
  `PHONE` char(255) DEFAULT NULL,
  `COUNTRY` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Add Users

INSERT INTO `USERS` VALUES ('1', 'Andrey', '18', '050-61-85-321', 'Ukraine', 'Kyiv');
INSERT INTO `USERS` VALUES ('2', 'Alexey', '25', '050-61-81-329', 'Ukraine', 'Odessa');
INSERT INTO `USERS` VALUES ('3', 'Nikolay', '41', '050-61-81-329', 'Russia', 'Moscow');
INSERT INTO `USERS` VALUES ('4', 'Marina', '25', '050-61-81-329', 'Ukraine', 'Kharkiv');
INSERT INTO `USERS` VALUES ('5', 'Anton', '19', '066-21-56-441', 'Poland', 'Warszawa');
INSERT INTO `USERS` VALUES ('6', 'Oleg', '24', '074-24-56-924', 'USA', 'NewYork');
INSERT INTO `USERS` VALUES ('7', 'Chris', '36', '063-54-22-445', 'Germany', 'Berlin');
INSERT INTO `USERS` VALUES ('8', 'Tommy', '25', '021-65-66-998', 'Italy', 'Roma');
INSERT INTO `USERS` VALUES ('9', 'Andrea', '45', '044-66-21-735', 'Spain', 'Madrid');
INSERT INTO `USERS` VALUES ('10', 'Paul', '33', '099-65-55-441', 'Poland', 'Lublin');