﻿<center><h2><a href="/php_info.php">Посмотреть информацию о php</a></h2><center>
<?php
//Парсим конфиг
$config = parse_ini_file("config.env");

//Создаем переменные с параметрами подключения к БД
$db_host = $config['MYSQL_HOST'];
$db_user = $config['MYSQL_USER'];
$db_pass = $config['MYSQL_PASSWORD'];
$db_base = $config['MYSQL_DATABASE'];

//Строка подключения к БД
$connection = mysqli_connect ($db_host, $db_user, $db_pass, $db_base);

//Запрос в БД
$sql_string = "SELECT * FROM USERS";

//Записываем Результат
$result = mysqli_query($connection,$sql_string);

echo "<center><h2>Список Клиентов:</h2></center>";

	//Рисуем таблицу
	echo '<center><table border="1">';
	echo '<thead>';
	echo '<tr>';
	echo '<th>ID</th>';
	echo '<th>Имя</th>';
	echo '<th>Возраст</th>';
	echo '<th>Телефон</th>';
	echo '<th>Страна</th>';
	echo '<th>Город</th>';
	echo '</tr>';
	echo '</thead>';
	echo '<tbody>';

//Цикл с выводом в таблицу результата
while($user = mysqli_fetch_assoc($result)) {
	echo '<tr>';
    echo '<td>' . "{$user['ID']}" . '</td>';
    echo '<td>' . "{$user['NAME']}" . '</td>';
    echo '<td>' . "{$user['AGE']}"." лет" . '</td>';
	echo '<td>' . "{$user['PHONE']}" . '</td>';
    echo '<td>' . "{$user['COUNTRY']}" . '</td>';
    echo '<td>' . "{$user['CITY']}" . '</td>';
    echo '</tr>';

}
?>