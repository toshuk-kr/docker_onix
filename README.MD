## �������� ������� Docker ����������� � ������������ �������������� php.ini �� ����������������� �����

### ����������:
1) ������������ ���� �����������:
```
git clone https://toshuk-kr@bitbucket.org/toshuk-kr/docker_onyx.git docker_onyx
```

2) ������� � ���������� � ��������:
```
cd docker_onyx
```

3) ��������� ������� ��� ������ � �������� �� ����������:
```
docker-compose build && docker-compose up -d
```
---

# ��������:

####������ ��� ������ � �������:####

`Docker:           18.06.1-ce`

`Docker-compose: 1.23.2, build 1110ad01`

#### **������:**

���� ������������ ����������� ������ � �����������: hub.docker.com

`https://hub.docker.com/_/php/`

`https://hub.docker.com/_/mysql/`

#### **��������� �������:**
```
+-- config.env
+-- db
��� +-- Dockerfile
��� L-- init.sql
+-- docker-compose.yml
+--  web
�   +-- docker-entrypoint.sh
��� L-- Dockerfile
L--  www
�   +-- index.php
�   L-- php_info.php
```
- **config_db.env** - ���������������� ���� ���������� ���� ������
- **config_web.env** - ���������������� ���� ���������� ���
- **db** - ���������� � ������ Dockerfile ��� ������ ������� ������� �� � ������ ���� ������ init.sql
- **docker-compose.yml** - ������������ ������ ����� �������
- **web** - ���������� � ������ Dockerfile ��� ������ ������� ������� ��� ������� � php � ������ ����� ��� ���������� docker-entrypoint.sh
- **www** - ���������� �� ������ php ���������� index.php � ������ php_info.php ��� ����������� ���������� �� PHP

#### **��������� ���������������� ������:**
###### **config_db.env:**
- **MYSQL_HOST=db** - ����� ������� ���������� MySQL
- **MYSQL_RANDOM_ROOT_PASSWORD=yes** - ������������ ������ root
- **MYSQL_USER=test** - ��� ������������ ��
- **MYSQL_PASSWORD=test** - ������ ������������ ��
- **MYSQL_DATABASE=test_db** - ��� ��

###### **config_web.env:**
- **APACHE_RUN_DIR=/var/run/apache2** - ���� � ������� ���������� ��� �������
- **APACHE_PID_FILE=/var/run/apache2/apache2.pid** - ���� � PID �����
- **APACHE_LOCK_DIR=/var/lock/apache2** - ���� � Lock ����������
- **APACHE_LOG_DIR=/var/log/apache2** - ���� � ���������� ����� ��� �������

 **��������� ������� php.ini:**
 
- **CFG_SHORT_OPEN_TAG=off** - ...
- **CFG_ASP_TAGS=off** - ...
- **CFG_PRECISION=14** - ...
- **...** 
---

#### **��� ������������ � ��� �������**
��� ����������� � ��� ������� ���������� ��������� bash ������� � ������� ������� �� ������� ����� ������ �������

```
sudo ifconfig | grep "inet addr"
```

**������ ���������� ���������� �������:**

```
inet addr:192.168.1.107  Bcast:192.168.1.255  Mask:255.255.255.0
```

- ��� 192.168.1.107 ��� ��������� IP �����, ������� ����� ������� http://192.168.1.107:80 � �������� ������ ��������

---